import json
import os
import pathlib
import jwt
import pytz

from datetime import datetime, timedelta
from typing import Union, Any

import uvicorn
from fastapi import FastAPI, HTTPException, Depends
from pydantic import BaseModel

from security import validate_token
from fastapi import Request

SECURITY_ALGORITHM = 'HS256'
SECRET_KEY = '123456'

app = FastAPI(
    title='FastAPI JWT', openapi_url='/openapi.json', docs_url='/docs',
    description='fastapi jwt'
)


class LoginRequest(BaseModel):
    username: str
    password: str


def verify_password(username, password):
    if username == 'admin' and password == 'admin':
        return True
    return False


def generate_token(username: Union[str, Any]) -> str:
    expire = datetime.utcnow() + timedelta(
        seconds=60 * 60 * 24 * 3  # Expired after 3 days
    )
    to_encode = {
        "exp": expire, "username": username
    }
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=SECURITY_ALGORITHM)
    return encoded_jwt


@app.get("/api/live")
async def get():
    return {
        "code": "000",
        "message": "Health check live success"
    }


@app.post('/api/hanet_webhook')
async def hanet_webhook(request: Request):
    if not os.path.exists(f"{pathlib.Path().resolve()}/datahanet.txt"):
        open(f"{pathlib.Path().resolve()}/datahanet.txt", "w")
    f = open(f"{pathlib.Path().resolve()}/datahanet.txt", "a")
    body = await request.body()
    data = body.decode("utf-8")
    now = datetime.now(tz=pytz.timezone("Asia/Ho_Chi_Minh"))
    f.write(f'{now} {data}\n')
    try:
        json.loads(data)
        f.write(f'{now} Json Format OK\n')
    except Exception as e:
        f.write(f'Error load json: {e}')
    return {'code': '000', 'message': 'Success'}


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8000)
